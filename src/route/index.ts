import express from 'express'
import scrap from '@route/scrap'
import generate from '@route/generate'

const router = express.Router()

router.use('/scrap', scrap)
router.use('/generate', generate)

export default router