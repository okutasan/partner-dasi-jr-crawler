import express from 'express'
import genPdf from '@service/generator/pdf'

const router = express.Router()

router.post('/pdf', (req, res) => {
  genPdf.scraper(req.body)
    .then((pdf) => {
      res.setHeader('Content-Type', 'application/pdf')
      res.setHeader('Content-Disposition', 'attachment; filename="your-pdf-file.pdf"')
      res.send(pdf).status(200)
    })
    .catch((err) => {
      res.status(500).json({message: 'FAILED_GENERATE_PDF', data: err})
    })
})

export default router