# Arutek's Web Scraper

## Installation
- Install NVM
- Install Node 18 LTS or higher (use LTS)
- Enable PNPM
- Run PNPM i
- Run docker using following command
```sh
$ sudo docker run -p \ <HOST_PORT>:<CONTAINER_PORT> --cap-add SYS_ADMIN --network mira-network --restart always --name web-scrapper -d novando/arutek:web-scrapper-<VERSION>
```